/**
 * Created by Mic on 16/2/24.
 */

module.exports = function (app) {
    return new DeskHandler(app);
};

var DeskHandler = function (app) {
    this.app = app;
};

var handler = DeskHandler.prototype;

handler.ready = function (msg, session, next) {
    this.app.rpc.room.deskRemote.ready(session, msg, next);
};

handler.jiao = function (msg, session, next) {
    this.app.rpc.room.deskRemote.jiao(session, msg, next);
};

handler.qiang = function (msg, session, next) {
    this.app.rpc.room.deskRemote.qiang(session, msg, next);
};

handler.double = function (msg, session, next) {
    this.app.rpc.room.deskRemote.double(session, msg, next);
};
handler.play = function (msg, session, next) {
    this.app.rpc.room.deskRemote.play(session, msg, next);
};
handler.hosting = function (msg, session, next) {
    this.app.rpc.room.deskRemote.hosting(session, msg, next);
};

handler.unHosting = function (msg, session, next) {
    this.app.rpc.room.deskRemote.unHosting(session, msg, next);
};
handler.test = function (msg, session, next) {
    this.app.rpc.room.deskRemote.test(session, msg, next);
};