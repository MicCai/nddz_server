var _poolModule = require('generic-pool');
var mysql = require('mysql');

var createMysqlPool = function (app) {
    var mysqlConfig = app.get('mysql');

    const factory = {
        create:function () {
            return new Promise(function (resolve,reject) {
                var client = mysql.createConnection({
                    host: mysqlConfig.host,
                    user: mysqlConfig.user,
                    password: mysqlConfig.password,
                    database: mysqlConfig.database
                });
                resolve(client);
            });
        },
        destroy:function (client) {
            return new Promise(function (resolve) {
                client.end();
                resolve();
            })
        }
    };

    var opts = {
        max:10,
        min:2
    };
    return _poolModule.createPool(factory,opts);
};

exports.createMysqlPool = createMysqlPool;