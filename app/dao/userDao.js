var pomelo = require('pomelo');

var userDao = module.exports;
userDao.getUserInfo = function (username, passwd, cb) {
    var sql = 'select * from  DOUDIZHU_USERS where username = ?';
    var args = [username];

    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        console.log('dbclient from mysql ', res, err);

        if (err !== null) {

        } else {
            var userId = 0;
            if (!!res && res.length === 1) {
                //var rs = res[0];
                console.log('select from mysql ', res);
            }
        }
    });
};

userDao.getUserByUuid = function (playerInfo, cb) {
    var uuid = playerInfo.uuid;
    var sql = 'select * from DOUDIZHU_USERS where uuid = ?';
    var args = [uuid];
    var self = this;
    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err) {
            //utils.invokeCallback(cb, err.message, null);
            console.error('dbclietn get info by uuid ' + err);
            //return;
        }
        if (!res || res.length <= 0) {
            self.createUser(uuid, playerInfo.username, 12000, function (err, res) {
                console.log('insert success ', playerInfo);
                playerInfo.coins = 12000;
                cb(null, 200, [playerInfo]);
            });
        } else {
            res[0].gender = playerInfo.gender;
            res[0].username = playerInfo.username;
            res[0].headUrl = playerInfo.headUrl;
            cb(err, 200, res);
        }
    });
};
userDao.createUser = function (uuid, username, coins, cb) {
    var sql = 'insert into DOUDIZHU_USERS (uuid,username,coins) values(?,?,?)';
    var args = [uuid, username, coins];
    pomelo.app.get('dbclient').insert(sql, args, function (err, res) {
        if (err !== null) {
            console.error('mysql insert error ' + err);
        } else {
            console.log('mysql insert success ' + username, res);
            cb(err, res);
        }
    });
};
userDao.updateMoney = function (playerInfo, coins, cb) {
    var sql = 'update DOUDIZHU_USERS set coins = ? where uuid = ?';
    var args = [coins, playerInfo.uuid];

    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err !== null) {
            console.error('mysql update error', err);
        } else {
            console.log('mysql update money success ', res);
            cb(err, res);
        }
    });
};
userDao.updateLoginTime = function (uuid, loginTime, getMoneyCounts, cb) {
    var sql = 'update DOUDIZHU_USERS set lastLoginTime = ?,getMoneyCounts = ? where uuid = ?';
    var args = [loginTime, getMoneyCounts, uuid];
    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err) {
            console.error('dbclietn get info by uuid ' + err);
            cb(err, {code: 500, uuid: uuid});
        } else {
            cb(err, res[0]);
        }
    });
};
userDao.updateGetMoneyCountsAndMoney = function (uuid, getMoneyCounts, money, cb) {
    var sql = 'update DOUDIZHU_USERS set getMoneyCounts = ?,coins = ? where uuid = ?';
    var args = [getMoneyCounts, money, uuid];
    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err) {
            console.error('dbclietn update getMoneyCounts info by uuid ' + err);
            cb(err, {code: 500, uuid: uuid});
        } else {
            cb(err, res[0]);
        }
    });
};
userDao.purchase = function (playerInfo, coins, cb) {
    var sql = 'update DOUDIZHU_USERS set coins = ? where uuid = ?';
    coins += parseInt(playerInfo.money);
    var args = [coins, playerInfo.uuid];

    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err !== null) {
            console.error('mysql update error', err);
        } else {
            console.log('mysql update money success ', res);
            cb(err, res);
        }
    });
};

userDao.deleteUser = function (uuid, cb) {
    var sql = 'delete from DOUDIZHU_USERS where uuid = ?';
    var args = [uuid];
    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err !== null) {
            console.error('mysql delete error', err);
        } else {
            console.log('mysql delete success ', res);
            cb(res);
        }
    });
};
userDao.getRandomName = function (id, cb) {
    var sql = 'select * from doudizhu_names where id = ?';
    var args = [id];
    pomelo.app.get('dbclient').query(sql, args, function (err, res) {
        if (err) {
            console.error('dbclietn get info by uuid ' + err);
            cb(err, {name: 'Rl_007'});
        } else {
            cb(err, res[0]);
        }
    });
};
