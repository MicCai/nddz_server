
1，memcached 安装教程  （在分布式的服务器之间用到了这个memcached 主要用于存储当前玩家的状态，比如在什么房间，掉线重连通过这个找到之前的房间）
            ======== 这个非常重要 一定要在服务器启动之前开启这个 ============

安装
https://github.com/memcached/memcached/wiki/Install
或者
http://www.cnblogs.com/yaoyao66123/p/4781042.html

启动
memcached -d -m 1024 -u root -l localhost -p 11211
或者
/usr/local/memcached/memcached-1.4.36/bin/memcached -d -m 1024 -u root -l localhost -p 11211



2，pomelo的安装
  1>,需在本机全局安装pomelo 具体安装参考网上教程。

3，pomelo的启动
    pomelo start -e development | production
    如果是产品模式下 要想在后台运行 加参数--daemon

4，pomelo已启动的服务器查看以及终止
    pomelo list -p (master端口)  或者 直接用 pomelo list
    pomelo list --port ()
    //关闭 pomelo 服务器
    pomelo stop -h 127.0.0.1 -p 3005 -u admin -p admin
    pomelo stop -h 127.0.0.1 --port 12000 -u admin -p admin



5，代码结构
    1，config文件夹 存放游戏的配置  场次配置表在room/room.json中
            {
                "name": "normal_room1",     //场次名
                "id": 1,                    //游戏场ID
                "baseBet": 100,             //基本倍数
                "money": 1000,              //最低要求money
                "ticket": 100,              //服务费
                "serverId": "room-server-1",//游戏场服务器ID
                "robot": true               //是否开启机器人
              },
        server.json中 是关于服务器的配置 上面是开发环境下配置 ，下面是正式环境下配置

    2，app 文件夹
        1> consts文件夹 存放一些游戏常量 与前端中的global中的对应上
        2> dao文件夹中
          userDao 编写了mysql的工具类，增删改查等接口 游戏类可以直接调用userDao中的函数
          memcached memcached的工具类

    3，domain 文件夹  游戏服务器的主要逻辑，每一块的逻辑相对应文件夹下面的

    4，servers 服务器文件夹

    5，util 频道 路由等工具类


有什么不明白的直接QQ问我。